/*

dict.js - dictionary loading
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import * as db from './database.js';

export const init = () => {
  return db.init().then((words) => {
    db.close()
    return new Promise( (resolve, reject) => {
      resolve()
    })
  }).catch((error) => {
    return new Promise( (resolve, reject) => {
      reject(new Error(`There was an error during initialization: ${error}`))
    })
  })
}

export function randomString(bag) {
  if(!Array.isArray(bag)) return bag;
  var res = bag[Math.floor(Math.random()*(bag.length))]
  if(res) return res
  return ''
}

export function getValue(type, inclusions, exclusions, formatter) {
  const words = db.find(type, inclusions, exclusions)

  let result = ''

  if(formatter) {
    result = formatter(randomString(words))
  } else {
    result = randomString(words)
  }

  return result
}

export const getNoun = (params) => {
  if(!params) params = {}
  const inclusions = params.inclusions
  const exclusions = params.exclusions
  const formatter = params.formatter

  let result = getValue('NOUN', inclusions, exclusions, formatter)

  return result
}

export const getPronoun = (params) => {
  if(!params) params = {}
  const inclusions = params.inclusions
  const exclusions = params.exclusions
  const formatter = params.formatter

  let result = getValue('PRONOUN', inclusions, exclusions, formatter)

  return result
}

export const getAdjective = (params) => {
  if(!params) params = {}
  var inclusions = params.inclusions
  var exclusions = params.exclusions
  var formatter = params.formatter

  let result = getValue('ADJECTIVE', inclusions, exclusions, formatter)

  return result
}

export const getVerb = (params) => {
  if(!params) params = {}
  var inclusions = params.inclusions
  var exclusions = params.exclusions
  var formatter = params.formatter

  let result = getValue('VERB', inclusions, exclusions, formatter)

  return result
}

export const getAdverb = (params) => {
  if(!params) params = {}
  var inclusions = params.inclusions
  var exclusions = params.exclusions
  var formatter = params.formatter

  let result = getValue('ADVERB', inclusions, exclusions, formatter)

  return result
}

export const getPreposition = (params) => {
  if(!params) params = {}
  var inclusions = params.inclusions
  var exclusions = params.exclusions
  var formatter = params.formatter

  let result = getValue('PREPOSITION', inclusions, exclusions, formatter)

  return result
}

export const getConjunction = (params) => {
  if(!params) params = {}
  var inclusions = params.inclusions
  var exclusions = params.exclusions
  var formatter = params.formatter

  let result = getValue('CONJUNCTION', inclusions, exclusions, formatter)

  return result
}

export const getInterjection = (params) => {
  if(!params) params = {}
  var inclusions = params.inclusions
  var exclusions = params.exclusions
  var formatter = params.formatter

  let result = getValue('INTERJECTION', inclusions, exclusions, formatter)

  return result
}
