#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import random
import sys

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

from inky.auto import auto

from PIL import Image, ImageFont, ImageDraw
from font_source_serif_pro import SourceSerifProSemibold
from font_source_sans_pro import SourceSansProSemibold

# Set up the correct display and scaling factors

inky_display = auto(ask_user=True, verbose=True)
inky_display.set_border(inky_display.WHITE)
# inky_display.set_rotation(180)

# This function will take a quote as a string, a width to fit
# it into, and a font (one that's been loaded) and then reflow
# that quote with newlines to fit into the space required.


def reflow_quote(quote, width, font):
    words = quote.split(" ")
    reflowed = '"'
    line_length = 0

    for i in range(len(words)):
        word = words[i] + " "
        word_length = font.getsize(word)[0]
        line_length += word_length

        if line_length < width:
            reflowed += word
        else:
            line_length = word_length
            reflowed = reflowed[:-1] + "\n  " + word

    reflowed = reflowed.rstrip() + '"'

    return reflowed


WIDTH = inky_display.width
HEIGHT = inky_display.height

# Create a new canvas to draw on

img = Image.new("P", (WIDTH, HEIGHT))
draw = ImageDraw.Draw(img)

# Load the fonts

font_size = 24

quote_font = ImageFont.truetype(SourceSansProSemibold, font_size)


# The amount of padding around the quote. Note that
# a value of 30 means 15 pixels padding left and 15
# pixels padding right.
#
# Also define the max width and height for the quote.

padding = 50
max_width = WIDTH - padding
max_height = HEIGHT - padding

below_max_length = False

def requests_retry_session(
    retries=3,
    backoff_factor=0.3,
    status_forcelist=(500, 502, 504),
    session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session
s = requests.Session()
response = requests_retry_session(session=s, retries=30).get('https://api.grunge.works/splork')

quote = ''

if response.status_code == 200:
  quote = response.json()['message']
else:
  print('Error fetching users: {}'.format(response.status_code))

# x- and y-coordinates for the top left of the quote

quote_x = (WIDTH - max_width) / 2
quote_y = (HEIGHT - max_height)

# Write our quote to the canvas

reflowed = reflow_quote(quote, max_width, quote_font)
draw.multiline_text((quote_x, quote_y), reflowed, fill=inky_display.BLACK, font=quote_font, align="left")

# Display the completed canvas on Inky wHAT

inky_display.set_image(img)
inky_display.show()
