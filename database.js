/*

database.js - load the database into memory
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import mariadb from 'mariadb';

const pool = mariadb.createPool({
	host: process.env.host,
	port: process.env.port,
	database: process.env.database,
	user: process.env.user,
	password: process.env.password,
	connectionLimit: 20
});

var wordsDB = []

export const init = async () => {
	let conn;
	try {
		conn = await pool.getConnection();
		const rows = await conn.query('SELECT word_value, type, categories FROM words');
		wordsDB = rows.map(row => {
			return {
				text: row.word_value,
				type: row.type,
				categories: row.categories.split('|'),
			}
		});
	} catch (err) {
		throw err;
	} finally {
		if (conn) conn.release();
	}
}

export const close = () => {
}

export const data = () => {
  return wordsDB
}

export const find = (type, inclusions, exclusions) => {
  let results = []
  if (wordsDB.length > 0) {
    results = wordsDB
                .filter( word => word.type === type)

    if (inclusions && Array.isArray(inclusions) && inclusions.length > 0) {
      results = results.filter( word => {
        let retVal = false
        const categories = word.categories
        if (!categories) {
          retVal = false
        } else if (Array.isArray(categories)) {
          if (categories.length === 0) {
            retVal = true
          } else {
            for (let catI=0; catI < categories.length; catI++) {
              let category = categories[catI]
              if (inclusions.includes(category)) {
                retVal = true
                break;
              }
            }
          }
        } else {
          retVal = inclusions.includes(categories)
        }

        return retVal
      })
    }

    if (exclusions && Array.isArray(exclusions) && exclusions.length > 0) {
      results = results.filter( word => {
        let retVal = true
        const categories = word.categories
        if (!categories) {
          retVal = true
        } else if (Array.isArray(categories)) {
          if (categories.length === 0) {
            retVal = true
          } else {
            for (let catI=0; catI < categories.length; catI++) {
              let category = categories[catI]
              if (exclusions.includes(category)) {
                retVal = false
                break;
              }
            }
          }
        } else {
          retVal = exclusions.includes(categories)
        }

        return retVal
      })
    }
  }
  return results
}
