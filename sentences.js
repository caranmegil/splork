/*

sentences.js - Sentence pattern filling
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import * as Dict from './dict.js';
import * as config from './config.js';

export const sentences = []

/*
 * The [random(<adjective>, .5)] <noun> [random(' in ' . <place>, .2)]
 * is <adjective>
 */
sentences.push(() => {
  var parts = []
  parts.push(deferString('the'))

  if(Math.random() >= 0.5) {
    parts.push(Dict.getAdjective())
  }

  parts.push(Dict.getNoun({ exclusions: ['PROPER']}))

  if(Math.random() <= 0.2) {
    parts.push(Dict.getNoun({
      formatter: function(result) {
        return 'in ' + getArticle(result) + result.text
      },
      inclusions: ['PLACE']
    }))
  }

  parts.push(deferString('is'))
  parts.push(Dict.getAdjective())

  return parts
})

/*
 * The [random(<adjective>, .5)] <noun> [random(' in ' . <place>, .2)]
 * is not <adjective>
 */
sentences.push(() => {
  var parts = []
  parts.push(deferString('the'))

  if(Math.random() >= 0.5) {
    parts.push(Dict.getAdjective())
  }

  parts.push(Dict.getNoun({ exclusions: ['PROPER']}))

  if(Math.random() <= 0.2) {
    parts.push(Dict.getNoun({
      formatter: function(result) {
        return 'in ' + getArticle(result) + result.text
      },
      inclusions: ['PLACE']
    }))
  }

  parts.push(deferString('is not'))
  parts.push(Dict.getAdjective())

  return parts
})

/* The <noun> from <place> will go to <place> */
sentences.push(() => {
  var parts = []
  parts.push(deferString('the'))

  parts.push(Dict.getNoun({ exclusions: ['PROPER']}))

  parts.push(deferString('from'))

  parts.push(Dict.getNoun({
    formatter: placeFormatter,
    inclusions: ['PLACE'],
  }))

  parts.push(deferString('will go to'))

  parts.push(Dict.getNoun({
    formatter: placeFormatter,
    inclusions: ['PLACE']
  }))

  return parts
})

/* <name> must take the <adjective> <noun> from <place> */
sentences.push(() => {
  var parts = []
  parts.push(Dict.getNoun({
    inclusions: ['PROPER']
  }))

  parts.push(deferString('must take the'))
  parts.push(Dict.getAdjective())
  parts.push(Dict.getNoun({ exclusions: ['PROPER']}))

  parts.push(deferString('from'))

  parts.push(Dict.getNoun({
    formatter: placeFormatter,
    inclusions: ['PLACE']
  }))

  return parts
})

/* <place> is <adjective> and the <noun> is <adjective> */
sentences.push(() => {
  var parts = []

  parts.push(Dict.getNoun({
    formatter: (result) => {
      return getArticle(result) + result.text
    },
    inclusions: ['PLACE']
  }))

  parts.push(deferString('is'))
  parts.push(Dict.getAdjective())
  parts.push(deferString('and the'))
  parts.push(Dict.getNoun({
    exclusions: ['PROPER']
  }))
  parts.push(deferString('is'))
  parts.push(Dict.getAdjective())

  return parts
})

/* <name> <preposition> <place> for the <adjective> <noun> */
sentences.push(() => {
  var parts = []

  parts.push(Dict.getNoun({
    inclusions: ['PROPER']
  }))

  parts.push(Dict.getPreposition())

  parts.push(Dict.getNoun({
    formatter: placeFormatter,
    inclusions: ['PLACE']
  }))

  parts.push(deferString('for the'))
  parts.push(Dict.getAdjective())
  parts.push(Dict.getNoun({
    exclusions: ['PROPER']
  }))

  return parts
})

/* The <noun> from <place> <action> the
 *   [random(<adjective> . ',' . [random(<adjective>, .2)], .5)]
 *   <noun>
 */
 sentences.push(() => {
   var parts = []

   parts.push(deferString('the'))
   parts.push(Dict.getNoun({
     exclusions: ['PROPER']
   }))
   parts.push(deferString('from'))

   parts.push(Dict.getNoun({
     formatter: placeFormatter,
     inclusions: ['PLACE']
   }))

   parts.push(Dict.getVerb())

   parts.push(deferString('the'))

   if(Math.random() < 0.5) {
     parts.push(Dict.getAdjective())
     if(Math.random() < 0.2) {
       parts.push(deferString(','))
       parts.push(Dict.getAdjective())
     }
   }

   parts.push(Dict.getNoun({
     exclusions: ['PROPER']
   }))

   return parts
 })

 /* The [random(<adjective>, .5)] <noun> <action> the <adjective> <noun>
  *  [random('in ' . <place>, .2]
  */
sentences.push(() => {
  var parts = []

  parts.push(deferString('the'))
  if(Math.random() < 0.5) {
    parts.push(Dict.getAdjective())
  }
  parts.push(Dict.getNoun({
    exclusions: ['PROPER']
  }))

  parts.push(Dict.getVerb())

  parts.push(deferString('the'))

  parts.push(Dict.getAdjective())
  parts.push(Dict.getNoun({
    exclusions: ['PROPER']
  }))

  if(Math.random() < 0.5) {
    parts.push(Dict.getNoun({
      formatter: function(result) {
        return 'in ' + getArticle(result) + result.text
      },
      inclusions: ['PLACE']
    }))
  }

  return parts
})

/* <name> <preposition> <place> and <action> the <noun> */
sentences.push(() => {
  var parts = []

  parts.push(Dict.getNoun({
    inclusions: ['PROPER']
  }))

  parts.push(Dict.getPreposition())

  parts.push(Dict.getNoun({
    inclusions: ['PLACE']
  }))

  parts.push(deferString('and'))
  parts.push(Dict.getVerb())
  parts.push(deferString('the'))
  parts.push(Dict.getNoun({
    exclusions: ['PROPER']
  }))

  return parts
})

// <name> takes <pronoun> [random(<adjective>, .5)] <noun> and
// <preposition> <place>
sentences.push(() => {
  var parts = []

  parts.push(Dict.getNoun({
    inclusions: ['PROPER']
  }))

  parts.push(deferString('takes'))
  parts.push(Dict.getPronoun())

  if(Math.random() < 0.5) {
    parts.push(Dict.getAdjective())
  }

  parts.push(Dict.getNoun( {exclusions: ['PROPER']}))

  parts.push(deferString('and'))
  parts.push(Dict.getPreposition())
  parts.push(Dict.getNoun({
    formatter: placeFormatter,
    inclusions: ['PLACE']
  }))

  return parts
})

/* <name> <action> the [random(<adjective>, .5)] <noun> */
sentences.push(() => {
  var parts = []

  parts.push(Dict.getNoun({
    inclusions: ['PROPER']
  }))

  parts.push(Dict.getVerb())

  parts.push(deferString('the'))

  if(Math.random() < 0.5) {
    parts.push(Dict.getAdjective())
  }

  parts.push(Dict.getNoun({exclusions: ['PROPER']}))

  return parts
})

/* <name> <action> <name> and <pronoun> [random(<adjective>, .5)] <noun> */
sentences.push(() => {
  var parts = []

  parts.push(Dict.getNoun({
    inclusions: ['PROPER']
  }))

  parts.push(Dict.getVerb({
    inclusions: ['ACTION']
  }))

  parts.push(Dict.getNoun({
    inclusions: ['PROPER']
  }))

  parts.push(deferString('and'))

  parts.push(Dict.getPronoun())

  if(Math.random() < 0.5) {
    parts.push(Dict.getAdjective())
  }

  parts.push(Dict.getNoun({exclusions: ['PROPER']}))

  return parts
})

/* The <noun> is the [random(<adjective>,.5)] <noun>; <name> <preposition> <place> */
sentences.push(() => {
  var parts = []
  parts.push(deferString('the'))

  parts.push(Dict.getNoun({
    exclusions: ['PROPER']
  }))

  parts.push(deferString('is the'))

  if(Math.random() < 0.5) {
    parts.push(Dict.getAdjective())
  }

  parts.push(Dict.getNoun({
    exclusions: ['PROPER']
  }))

  parts.push(deferString(';'))

  parts.push(Dict.getNoun({
    inclusions: ['PROPER']
  }))

  parts.push(Dict.getPreposition())

  parts.push(Dict.getNoun({
    formatter: placeFormatter,
    inclusions: ['PLACE']
  }))

  return parts
})

/* You must meet <name> at <place> and retrieve the [random(<adjective>, .5)] <noun> */
sentences.push(() => {
  var parts = []
  parts.push(deferString('you must meet'))

  parts.push(Dict.getNoun({
    inclusions: ['PROPER']
  }))

  parts.push(deferString('at'))
  parts.push(Dict.getNoun({
    formatter: placeFormatter,
    inclusions: ['PLACE']
  }))
  parts.push(deferString('and retrieve the'))

  if(Math.random() < 0.5) {
    parts.push(Dict.getAdjective())
  }

  parts.push(Dict.getNoun({
    exclusions: ['PROPER']
  }))

  return parts
})

/* Without the [random(<adjective>,.3)] <noun>, the <noun> <preposition> <place>! */
sentences.push(() => {
  var parts = []
  parts.push(deferString('without the'))

  if(Math.random() < 0.5) {
    parts.push(Dict.getAdjective())
  }

  parts.push(Dict.getNoun({
    exclusions: ['PROPER']
  }))

  parts.push(deferString(', the'))

  parts.push(Dict.getNoun({
    exclusions: ['PROPER']
  }))

  parts.push(Dict.getPreposition())

  parts.push(Dict.getNoun({
    formatter: placeFormatter,
    inclusions: ['PLACE']
  }))

  parts.push(deferString('!'))
  return parts
})

////
// Util functions
////

function placeFormatter(result, prefix) {
  var res = getArticle(result)
  if(result && result.text)
    res += result.text
  return res
}

function deferString(msg) {
  return msg
}

function getArticle(result) {
  if(result.categories && result.categories.indexOf('PROPER') >= 0) {
    return ''
  } else {
    return 'the '
  }
}

export const generate = (success) => {
  var parts = []
  if(Math.random() < config.INTRO_CHANCE) {
    parts.push(Dict.getInterjection())
  }

  parts =  parts.concat(sentences[Math.floor(Math.random()*(sentences.length))]())

    var response = parts.map((res) => {
      var retVal = ''
      if(res) {
        if(res.text) {
          retVal = res.text
        } else {
          retVal = res
        }
      }

      return retVal
    }).join(' ').split(' ')
    .reduce((prev, curr) => {
      let reduced = prev
      if(['!', '?', '.'].indexOf(reduced[reduced.length-1]) >= 0) {
        reduced += '  ' + curr[0].toUpperCase() + curr.substr(1)
      } else if(['!', '?', '.', ';', ','].indexOf(curr) >= 0) {
        reduced += curr
      } else {
        reduced += ' ' + curr
      }
      return reduced
    }).trim()

    if(['!', '?', '.'].indexOf(response[response.length-1]) < 0) {
      if(Math.random() < config.EXCL_CHANCE) {
        response += '!'
      } else {
        response += '.'
      }
    }

    response = response[0].toUpperCase() + response.substr(1)
      
    return response
}
