/*

index.js - another driver
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import Koa from 'koa'
import Router from 'koa-router';
import serve from 'koa-static';
import mount from 'koa-mount';
import 'dotenv/config';

import * as sentences from './sentences.js';
const generate = sentences.generate;

import * as dict from './dict.js';

// init database and query every 30 seconds for updates
dict.init();
setInterval( function() {
    dict.init();
}, 30000);

const app = new Koa();
const router = new Router();

router.get('/message', async (ctx, next) => {
    const message =  generate();
    ctx.body = {
        message,
    };
});

router.get('/motd', async (ctx, next) => {
    const message = generate();
    ctx.body = {
        message,
    };
});

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(process.env.PORT || 3000);
