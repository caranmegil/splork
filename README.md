# Splork
Splork is a random sentence generator that pulls data from a specialized realtime database called Firebase.  It generates randomly from a selection of sentence structures.  Splork will fill in each part of speech with a random work.  Appreciate the random for some good sometimes clean fun!
## Installation
Installation is simple.  Install the latest and greatest Node.  Next, run npm install.  Next, fill in the following environment variables:
* serviceAccountKey - the Firebase key giving read/write access to the your Firebase database.
* databaseURL - the URL to your Firebase database.
* PORT - the port to run the application on (optional, defaults to 3000)
Finally, run node index.js to start.
## TODO
I need to get the application to support a different realtime database that can be hosted locally.  Let the exploration begin!